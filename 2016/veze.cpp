
#include "zasobnik.h";

#include <iostream>;

using namespace std;

void init(zasobnik *Z,int n)
{
	for (int i = 0; i < n; i++)
		Z->push(n-i);
		
}

void Hanoj(int vel, zasobnik * Z, zasobnik * C, zasobnik *O, zasobnik *A, zasobnik *B, zasobnik *CC)
{

	if (vel == 1)
	{
		C->push(Z->pull());
		cout << endl;
		cout << "A:";
		A->vypis();
		cout << "B:";
		B->vypis();
		cout << "C:";
		CC->vypis();
		cout << endl;

	}
	else
	{
		Hanoj(vel - 1, Z, O, C,A,B,CC);
		C->push(Z->pull());

		cout << endl;
		cout << "A:";
		A->vypis();
		cout << "B:";
		B->vypis();
		cout << "C:";
		CC->vypis();
		cout << endl;

		Hanoj(vel - 1, O, C, Z,A,B,CC);
	}




}


int main(void)
{

	zasobnik A;
	zasobnik B;
	zasobnik C;

	init(&A,4);

	cout << "A:" ;
	A.vypis();
	cout << "B:";
	B.vypis();
	cout << "C:";
	C.vypis();

	Hanoj(4, &A, &B, &C, &A, &B, &C);

	cout << "A:";
	A.vypis();
	cout << "B:";
	B.vypis();
	cout << "C:";
	C.vypis();

	system("pause");

	return 0;
}