
const int N = 8;

struct Pozice
{
    Pozice( int i, int j )
    {
        sloupec = i;
        radek = j;
    }
    
    int sloupec, radek;
};

const int skoky[8][2] = {{2, 1}, {2, -1}, {1, -2} ,... };

bool skok_konem( int sachovnice[ N ][ N ], Pozice *pozice, int *cislo_skoku )
{
    while( *cislo_skoku < 8 )
    {
        pozice->sloupec += skoky[ *cislo_skoku ][0];
        pozice->radek += skoky[ *cislo_skoku ][1];
        *cislo_skoku++;
        if( pozice->radek < N && pozice->radek >= 0 &&
            pozice->sloupec < N && pozice->sloupec >= 0 &&
            sachovnice[pozice->sloupec][pozice->radek] == 0 )
           return true;
        pozice->sloupec -= skoky[ *cislo_skoku - 1 ][0];
        pozice->radek -= skoky[ *cislo_skoku - 1 ][1];
    }
    return false;
}

bool sachovyKun( int sachovnice[ N ][ N ],
                 Pozice cesta[ N * N ], int krok )
{
    Pozice pozice = cesta [krok];
    if(krok== N * N - 1)
        return true;
    int cislo_skoku = 0;
    while(skok_konem(sachovnice, &pozice, &cislo_skoku))
    {
        krok++;
        sachovnice[pozice.sloupec][pozice.radek] = krok;
        cesta[krok]=pozice;
        if(sachovyKun(sachovnice,cesta,krok))
            return true;
        sachovnice[pozice.sloupec][pozice.radek] = 0;
        krok--;
        pozice = cesta[krok];
    }
    return false;
}


int main( int argc, char** argv )
{
    int sachovnice[ N ][ N ];
    Pozice cesta[ N * N ];
    for( int i = 0; i < N; i++ )
        for( int j = 0; j < N; j++ )
            sachovnice[ i ][ j ] = 0;
    
    sachovnice[ 3 ][ 5 ] = 1;
    cesta[ 0 ] = Pozice( 3, 5 );
    sachovyKun( sachovnice, cesta, 0 );
    return 0;
}

