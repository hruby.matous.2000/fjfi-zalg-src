#ifndef MUJ_STROM_H
#define MUJ_STROM_H

struct Prvek {
	int Data;
	Prvek * L;
	Prvek * P;
};

class Strom
{
public:
	Strom();
	bool Vloz(int D); //vrac� false kdy� prvek ji� ve stromu byl
	bool Smaz(int D); //vrac� false kdy� prvek ve stromu nebyl
	bool Najdi(int D); //vrac� true pokud prvek ve stromu je
	~Strom();

private:
	Prvek * koren;
	bool Najdi(int D,Prvek *podstrom,Prvek **predchudce,Prvek **hledany);


};






#endif