#include "zasobnik.h"
#include <iostream>
using namespace std;

zasobnik::zasobnik(){
	hlava = nullptr;
}
zasobnik::~zasobnik(){
	while (hlava != nullptr){
		pull();
	}
}
void zasobnik::push(int data){
	prvek *pom;
	pom = new prvek;
	pom->data = data;
	pom->dalsi = hlava;
	hlava = pom;
}
int zasobnik::pull(void){
	prvek *pom;
	if (hlava == nullptr){
		cerr << "prazdny zasobnik" << endl;
		return 0;
	}
	pom = hlava;
	hlava = pom->dalsi;
	int d = pom->data;
	delete pom;
	return d;
}
void zasobnik::vypis2(void){
	prvek *pom;
	pom = hlava;
	while (pom != nullptr){
		cout << pom->data <<"\t";
		pom = pom->dalsi; 
	}
	cout << endl;
}

void zasobnik::vypis(void){
	prvek *pom;
	pom = hlava;
	zasobnik tmp;
	while (pom != nullptr){
		tmp.push(pom->data);
		pom = pom->dalsi;
	}
	tmp.vypis2();
}