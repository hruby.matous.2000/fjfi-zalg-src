bool otestujHodnotu( int pole[9][9],
                     int hodnota,
                     int radek,
                     int sloupec )
{
    for(int i = 0; i < 9; i++){
        if(pole[radek][i] == hodnota || pole[i][sloupec] == hodnota){
            return false;
        }        
    }
    int a = 3*(radek/3), b = 3*(sloupec/3);
    for(int i = a; i < a + 3; i++){
        for(int j = b; j < b + 3; j++){
            if(pole[i][j] == hodnota){
                return false;
            }
        }
    }
    return true;
}

bool sudoku( int pole[9][9],
             bool zadane[9][9] )
{
    int radek=0,sloupec=0;
    while(radek<9)
    {
        if(zadane[radek][sloupec]==true)
        {
            sloupec++;
            if(sloupec==9)
            {
                radek++;sloupec=0;
            }
            continue;
        }
        int hodnota = pole[radek][sloupec]+1;
        while(hodnota <= 9 && 
              otestujHodnotu(pole,hodnota,radek,sloupec)!=true)
            hodnota++;
        
        if(hodnota==10)
        {
            while(zadane[radek][sloupec]==true || pole[radek][sloupec]==9)
            {
                sloupec--;
                if(sloupec==-1)
                {
                    radek--;sloupec=8;
                    if(radek==-1)
                        return false;
                }
            }
            continue;
        }
        pole[radek][sloupec]=hodnota;
        sloupec++;
        if(sloupec==9)
        {
            radek++;sloupec=0;
        }
        continue;        
    }
    return true;
}