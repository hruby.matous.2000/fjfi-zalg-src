#include "binarni-strom.h"


 Strom::Strom()
 {
     koren = NULL;
 }
    
 bool Strom::vloz( double data )
 {
     if(koren == NULL){
         koren = new Uzel;
         koren->data = data;
         koren->levy = NULL;
         koren->pravy = NULL;
     }
     else
         vlozDoPodstromu( koren, data );
 }
    
 bool Strom::vlozDoPodstromu( Uzel* podstrom, double data )
 {
     if( data < podstrom->data )
     {
         if( podstrom->levy == NULL )
         {
            Uzel* novy = new Uzel;
            novy->data = data;
            novy->levy = NULL;
            novy->pravy = NULL;
            podstrom->levy = novy;
            return true;
         }
         return vlozDoPodstromu( podstrom->levy, data );
     }
     if ( data > podstrom->data )
     {
         if( podstrom->pravy == NULL )
         {
            Uzel* novy = new Uzel;
            novy->data = data;
            novy->levy = NULL;
            novy->pravy = NULL;
            podstrom->pravy = novy;
            return true;
         }
         return vlozDoPodstromu( podstrom->pravy, data );
     }
     return false;
 }
 
 bool Strom::najdi( double data )
 {
     if (koren == NULL)
     { return false;  }
     else 
        return najdiVeStromu (koren, data)
 }
    
 bool Strom::najdiVeStromu( Uzel* strom, double data )
 {
     if (strom == NULL)
         return false;
     if ( strom->data == data)
         return true;
     if (strom->data > data)
         return najdiVeStromu (strom->levy, data );
     else 
         return najdiVeStromu (strom->pravy, data);
     
 }
 
 Uzel* Strom::najdiUzelVeStromu( Uzel* strom, double* data, Uzel** predchudce )
 {
     *predchudce = strom;
     *data = 5;
 }
 
 bool Strom::smaz( double data )
 {
     Uzel* predchudce = NULL;
     Uzel* uzel = najdiUzelVeStromu( koren, &data, &predchudce );
 }
    
 bool Strom::zapis( const char* soubor )
 {
     
 }
    
 bool Strom::nacti( const char* soubor )
 {
     
 }
    
 Strom::~Strom()
 {
     
 }         