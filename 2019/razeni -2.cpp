// razeni.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

const N = 100;

void primeVkladani(double a[N] )
{
	for (int j = 0; j < N-1; j++)
	{
		int index = j;
		for (int i = j+1; i < N; i++)
		{
			if (a[index] > a[i])
				index = i;
		}
		double pom;
		pom = a[j];
		a[j] = a[index];
		a[index] = pom;
	}
}

void primyVyber(double a[N])
{
	for (int i = 1; i < N; i++)
	{
		double pom = a[i];
		int j = i - 1;
		while (pom < a[j] && j>0 )
		{
			a[j+1] = a[j];
			j--;
		}
		a[j] = pom;
	}
}

void pretrasani(double a[N])
{
	int levy = 0, pravy = N, posledni;
	for (int i = levy; i < pravy; i++)
	{
		double tmp;
		if (a[i] > a[i + 1])
		{
			tmp = a[i];
			a[i] = a[i + 1];
			a[i + 1] = tmp;
			posledni = i + 1;
		}
	}
	pravy = posledni;
	for (int i = pravy-1; i > levy; i--)
	{
		double tmp;
		if (a[i] < a[i - 1])
		{
			tmp = a[i];
			a[i] = a[i - 1];
			a[i - 1] = tmp;
			posledni = i - 1;
		}
	}
	levy = posledni;
}

void quicksort(double a[N], int zacatek = 0, int konec = N)
{
	double pivot = a[konec-1];
	int leva = zacatek;
	int prava = konec - 2;
	while (leva < prava)
	{
		while (a[leva] < pivot && leva<prava)
			leva++;
		while (a[prava] > pivot && leva < prava)
			prava--;
		if (leva != prava)
		{
			double tmp = a[leva];
			a[leva] = a[prava];
			a[prava] = tmp;
		}
	}
	if (a[leva] < pivot)
	{
		a[konec - 1] = a[leva+1];
		a[leva+1] = pivot;
	}
	else
	{
		a[konec - 1] = a[leva];
		a[leva] = pivot;
	}
	if( zacatek < leva - 1 )
		quicksort(a, zacatek, leva);
	if( leva + 2 < konec )
		quicksort(a, leva + 1, konec);
}

void vlozNaHaldu(double a[N], int zacatek, int konec)
{
	int i = zacatek;
	while(true)
	{
	
		int levy = 2 * i + 1;
		if (levy >= konec)
			break;
		int pravy = 2 * i + 2;
		int max = levy;
		if (pravy < konec && a[max] < a[pravy])
			max = pravy;
		if (a[max] > a[i])
		{
			prohod(&a[max], &a[i]);
			i = max;
		}
		else
			break;
	}
}
void heapsort(double a[N])
{
	int i = std::floor( N / 2 ) - 1;
	while (i>=0)
	{
		vlozNaHaldu(a, i, N);
		i--;
	}
	int i = N-1;
	while (i > 0)
	{
		prohod(&a[i], &a[0]);
		vlozNaHaldu(a, 0, i);
		i--;
	}
}

const int pocetCifer = ...;
int cifra(int cislo, int poziceCifry) { ... };
void radixSort( int a[N])
{
	int pom[R + 1];
	int pole[N];
	for (int j = 0; j < pocetCifer; j++)
	{
		for (int i = 0; i <= R; i++)
		{
			pom[i] = 0;
		}
		for (int i = 0; i < N; i++)
		{
			pom[cifra(a[i], j) + 1] += 1;
		}
		for (int i = 1; i <= R; i++)
		{
			pom[i] = pom[i] + pom[i - 1];
		}
		for (int i = 0; i < N; i++)
		{
			int b = cifra(a[i], j);

			pole[pom[b]] = a[i];
			pom[b] += 1;
		}
		for (int i = 0; i < N; i++)
		{
			a[i] = pole[i];
		}
	}
}

void radixSortCompact(int a[N])
{
	int pom[R + 1];
	int pole[N];
	for (int j = 0; j < pocetCifer; j++)
	{
		for (int i = 0; i <= R; i++) pom[i] = 0;
		for (int i = 0; i < N; i++) pom[cifra(a[i], j) + 1]++;
		for (int i = 1; i <= R; i++) pom[i]++;
		for (int i = 0; i < N; i++) pole[pom[cifra(a[i], j)]++] = a[i];
		for (int i = 0; i < N; i++) a[i] = pole[i];
	}
}

void mergeSort(double a[N], int zacatek = 0, int konec = N)
{
	int prostredek = (zacatek + konec) / 2;
	if( prostredek - zacatek > 1 )
		mergeSort(a, zacatek, prostredek);
	if( konec - prostredek > 1 )
		mergeSort(a, prostredek, konec);
	int leva = zacatek;
	int prava = prostredek;
	double pole[N];
	int i = zacatek;
	while (leva < prostredek || prava < konec)
	{
		if (leva < prostredek && a[leva] < a[prava])
			pole[i++] = a[leva++];
		if (prava < konec && a[prava] < a[leva])
			pole[i++] = a[prava++];
	}
	for (int i = zacatek; i < konec; i++)
		a[i] = pole[i];
}

int main()
{
	double a[N];
	quicksort(a);

}












































