#include "pch.h"
#include <iostream>

const int N = 9;
int sudoku[N][N];
bool zadane[N][N];

bool dalsi(int *radek, int *sloupec)
{
	while (zadane[*radek][*sloupec] == true)
	{
		if (*sloupec < N-1)
			*sloupec++;
		else
		{
			*sloupec = 0;
			*radek++;
		}
		if (*radek > N - 1)
			return false;
	}
	return true;
}

bool predchozi(int *radek, int *sloupec)
{
	while (zadane[*radek][*sloupec] == true)
	{
		if (*sloupec > 0)
			*sloupec--;
		else
		{
			*sloupec = N-1;
			*radek--;
		}
		if (*radek < 0)
			return false;
	}
	return true;
}

bool je_pripustna(int radek, int sloupec, int hodnota)
{
	for (int i = 0; i < N; i++)
	{
		if (sudoku[radek][i] == hodnota ||
			sudoku[i][sloupec] == hodnota )
			return false;
	}
	int N_sqrt = sqrt(N);
	int prvni_radek = (radek / N_sqrt)*N_sqrt;
	int prvni_sloupec = (sloupec / N_sqrt)*N_sqrt;

	for(int i=prvni_radek;i<prvni_radek+N_sqrt;i++)
		for (int j = prvni_sloupec; j < prvni_sloupec + N_sqrt; j++)
		{
			if (sudoku[i][j] == hodnota)
				return false;
		}
	return true;
}

bool resSudoku()
{
	int radek = 0;
	int sloupec = 0;
	
	dalsi(&radek, &sloupec);
	while ( true )
	{
		int i;
		for( i=1 ; i <= N; i++)
		{
			if (je_pripustna(radek, sloupec, i) == true)
			{
				sudoku[radek][sloupec] = i;
				break;
			}
		}
		if (i > N)
		{
			if (!predchozi(&radek, &sloupec))
				return false;
			sudoku[radek][sloupec] = 0;
		}
		else
		{
			if (!dalsi(&radek, &sloupec))
				return true;
		}
	}
}


int main()
{
	for( int i = 0; i < N; i++ )
		for (int j = 0; j < N; j++)
		{
			sudoku[i][j] = 0;
			zadane[i][j] = false;
		}
	// Zadani 
	// ....
	//
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
		{
			if( sudoku[i][j] != 0 )
				zadane[i][j] = true;
		}
	resSudoku();
}

