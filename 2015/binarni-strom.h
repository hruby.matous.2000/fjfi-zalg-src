#include <assert.h>

struct UzelStromu
{
   int data;

   UzelStromu *levy, *pravy;

   UzelStromu( const int hodnota )
   {
      data = hodnota;
      levy = NULL;
      pravy = NULL;
   }
};

struct BinarniStrom
{
   UzelStromu *koren;

   BinarniStrom()
   {
      koren = NULL;
   }

   bool pridejPrvek( const int hodnota );

   bool najdiPrvek( const int hodnota );

   void smazPrvek( const int hodnota );

   ~BinarniStrom();

   
   /****
    * Pomocne metody
    */
   protected:

   bool hledejPrvek( const UzelStromu* podstrom,
                     const int hodnota,
                     UzelStromu** predchudce,
                     UzelStromu** prvek );
     
};

bool BinarniStrom::pridejPrvek( const int hodnota )
{
   if  (koren == NULL)
      {
         koren = new UzelStromu( hodnota );
         return true;
      }
   if (hodnota < koren->data)
      {
      }
}