type Graf = object

public

constructor vytvor( pocetVrcholu : integer );

function pocetSousedu( v : Vrchol ) : integer;

function soused( v : vrchol, i : integer ) : Vrchol;

function delkaHrany( u, v : vrchol ): integer;

function velikostGrafu() : integer;

destructor zrus();

end;

function Dijkstra( start, cil : vrchol, var g : graf, var cesta array of vrchol ) : integer
var vzdalenosti ^integer;
    znacky ^znacka; {znacka je bud znama, pokusna nebo neznama}
    i : integer;
    pokusny:
begin
   new( vzdalenosti, g. velikostGrafu() );
   new( znacky, g. velikostGrafu() );

   for i := 1 to g. velikostGrafu() do
      vzdalenosti[ i ] := INT_MAX;
      znacky[i]:=neznama;

   vzdalenosti[start. index ]:=0;
   znacky[start. index ]:=pokusna;


   while znacky[cil.index]<>znama do
   begin

     for i:=1 to g.velikostgrafu do
        if znacky[i]=pokusna then
           if vzdalenosti[i]<nejmensi then
           begin
              nejmensi:=vzdalenosti[i];
              nejmensi_vrchol:=i;
           end;

         znacky[nejmensi_vrchol]:=znama;

     for i:=1 to g.pocetSousedu(nejmensi_vrchol) do
      begin
        soused(i, nejmensi_vrchol)
      end;

      end;

end;


