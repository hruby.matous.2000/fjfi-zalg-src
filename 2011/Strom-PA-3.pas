program Strom;

type ukVrchol = ^Vrchol;
Vrchol = record
       data : integer;
       levy, pravy : ukVrchol;
end;

type Strom = object
     koren : ukVrchol;

     constructor vytvor;

     procedure vloz( data : integer );

     function hledej( data : integer ) : boolean;

     procedure smaz( data : integer );

     procedure uloz( soubor : file of integer );

     procedure nacti( soubor : file of integer  );

     destructor smazStrom();

     {pomocne metody}

     function novyVrchol( data : integer ) : ukVrchol;

     procedure vlozDoStromu( data : integer, korenPodstromu : ukVrchol );

     procedure smazPodstrom( korenPodsttromu : ukVrchol );

     function hledejVPodstromu( data : integer, korenPodstromu : ukVrchol, var predchozi : ukVrchol ) : ukVrchol;

     procedure smazList( list, predchozi : ukVrchol );

     procedure smazVeVetvi( list, predchozi : ukVrchol );

     procedure smazVeStromu (vrchol: UkVrchol );

     procedure smazPrvek( vrchol, predchozi : ukVrchol );

     procedure ulozPodstrom( vrchol : ukVrchol, soubor : file of integer  );


end;

constructor Strom. vytvor
begin
     koren := nil;
end;

function Strom. novyVrchol( data : integer ) : ukVrchol
begin
     new( novyVrchol );
     novyVrchol^. data  := data;
     novyVrchol^. levy  := nil;
     novyVrchol^. pravy := nil;
end;

procedure Strom. vloz( data : integer )
begin
     vlozDoStromu( data, koren );
end;

procedure Strom. vlozDoStromu( data : integer, var korenPodstromu : ukVrchol )
begin
     if korenPodstromu = nil then
        korenPodstromu := novyVrchol( data )
     else
     begin
          if korenPodstromu^. data < data then
             vlozDoStromu(data, korenPodstromu^.levy)
          if korenPodstromu^. data > data then
             vlozDoStromu(data, korenPodstromu^.pravy)
     end;
end;

procedure Strom. smazPodstrom( korenPodstromu : ukVrchol )
begin
     if korenPodstromu^. levy <> nil then
        smazPodstrom( korenPodstromu^. levy );
     if korenPodstromu^. pravy <> nil then
        smazPodstrom( korenPodstromu^. pravy );
     dispose( korenPodstromu );
end;

function Strom. hledej( data : integer ) : boolean
var predchozi : ukPrvek;
begin
     if koren <> nil then
        if hledejVPodstromu( data, koren, predchozi ) = nil then
           hledej := false
        else hledej:= true;
     else
         hledej := false;

end;

function Strom. hledejVPodstromu( data : integer,
                                  korenPodstromu : ukVrchol,
                                  var predchozi : ukVrchol ) : ukVrchol
begin
     if data = korenPodstromu^. data then hledejVPodstromu := korenPodstromu;
     if data < korenPodstromu^.data then
        if korenPodstromu^.levy <> nil then
        begin
             predchozi := korenPodstromu;
             hledejVPodstromu := hledejVPodstromu(data, korenPodstromu^.levy, predchozi )
        end
        else hledejVPodstromu := nil;
     if data> korenPodstromu^.data then
        if korenPodstromu^.pravy <> nil then
        begin
             predchozi := korenPodstromu;
             hledejVPodstromu:=hledejVPodstromu(data, korenPodstromu^.pravy, predchozi )
        end
        else hledejVPodstromu := nil;
end;

procedure Strom. smazList( list, predchozi : ukVrchol )
begin
     {assert( list=predchozi^.levy or list=predchozi^.pravy);}
     {assert( list^.levy = nil and list^.pravy = nil);}
     if list=predchozi^.levy then
        predchozi^.levy := nil
     else
         predchozi^.pravy := nil;
     dispose(list);
end

procedure Strom. smazVeVetvi( list, predchozi : ukVrchol )
var
   nasledujici: ukVrchol;
begin
     {assert( list=predchozi^.levy or list=predchozi^.pravy);}
     {assert( list^.levy <> nil or list^.pravy <> nil);}
     {assert( not (list^.levy <> nil and list^.pravy <> nil)))}
     if list^.levy = nil then
        nasledujici:= list^.pravy
     else
         nasledujici:= list^.levy;
     if list=predchozi^.levy then
        predchozi^.levy := nasledujici
     else
         predchozi^.pravy := nasledujici;
     dispose(list);
end;

procedure Strom. smazVeStromu (vrchol: UkVrchol)
var pomocny,predchozi: UkVrchol
begin
     pomocny:=vrchol^.pravy;
     predchozi := vrchol;
     while pomocny^.levy <> nil do
     begin
          predchozi:= pomocny;
          pomocny := pomocny^.levy;
     end
     vrchol^.data := pomocny^.data ;
     smazPrvek( pomocny, predchozi );
end

procedure Strom. smazPrvek( vrchol, predchozi : ukVrchol )
begin
   if (vrchol^.levy = nil) and (vrchol^.pravy = nil) then
      {Mazani listu}
      smazlist(vrchol, predchozi);
   else
       if (vrchol^.levy <> nil) and (vrchol^.pravy <> nil) then
       begin
            {Mazani ve stromu}
            smazVeStromu( vrchol );
       end
       else
       begin
            {Mazani ve vetvi}
            smazVeVetvi(vrchol, prechozi);
       end
   end;
end

procedure Strom. smaz( data : integer )
var vrchol, predchozi, pomocny : ukVrchol
begin
vrchol:= hledejvpodstromu( data, koren, predchozi );
if vrchol <> nil then
   smazPrvek( vrchol, predchozi );
end

destructor Strom. smazStrom()
begin
     if koren <> nil then
        smazPodstrom( koren );
     koren := nil;
end;

procedure Strom. uloz( soubor : file of integer )
begin
     ulozPodstrom( koren, soubor );
end

procedure Strom. ulozPodstrom( vrchol : ukVrchol, soubor : file of integer )
begin
     if vrchol <> nil then
     begin
          write( soubor, vrchol^. data );
          ulozPodstrom( vrchol^. levy, soubor );
          ulozPodstrom( vrchol^. pravy, soubor );
     end
end

procedure Strom. nacti( soubor : file )
var data : integer
begin
     smazStrom;
     while not eof( soubor ) do
     begin
          read( soubor, data );
          vloz( data );
     end
end








