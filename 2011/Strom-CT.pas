program Strom;

type ukVrchol = ^Vrchol;
Vrchol = record
       data : integer;
       levy, pravy : ukVrchol;
end;

type Strom = object
     koren : ukVrchol;

     constructor vytvor;

     procedure vloz( data : integer );

     function hledej( data : integer ) : boolean;

     procedure smazPrvek( data : integer );

     destructor smazStrom();

     {pomocne metody}

     function novyVrchol( data : integer ) : ukVrchol;

     procedure vlozDoStromu( data : integer, korePodstromu : ukVrchol );
end;

constructor Strom. vytvor()
begin
     koren := nil;
end;

function Strom. novyVrchol( data : integer ) : ukVrchol
begin
     new( novyVrchol );
     novyVrchol^. data  := data;
     novyVrchol^. pravy := nil;
     novyVrchol^. levy  := nil;
end;

procedure Strom. vlozDoStromu( data : integer, korenPodstromu : ukVrchol )
begin
     if data < korenPodstromu^.data then begin
        if korenPodstromu^.levy = nil then
           korenPodstromu^.levy := novyVrchol( data );
        else
            vlozDoStromu( data, koren^.levy );
        end
    end;
    if data > korenPodstromu^.data then begin
        if korenPodstromu^.pravy = nil then
           korenPodstromu^.pravy := novyVrchol( data );
        else
            vlozDoStromu( data, koren^.pravy );
        end
    end;
end;

procedure Strom. vloz( data : integer )
begin
     if koren = nil then
        koren := novyVrchol( data );
     else
         vlozDoStromu( data, koren );
     end;
end;

procedure Strom.smazPodstrom(korenPodstromu : ukVrchol)
begin
     if korenPodstromu^.levy <> nil then
        smazStrom( korenPodstromu^.levy)
     if korenPodstromu^.pravy <> nil then
        smazStrom(korenPodstromu^.pravy)
     dispose(korenPodstromu);
end;

destructor Strom. smazStrom()
begin
     if koren <> nil then
        smazPodstrom(koren)
end;














