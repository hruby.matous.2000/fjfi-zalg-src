type
 UkPrvekSeznamu = ^PrvekSeznamu;
 PrvekSeznamu = record
    data : integer
    nasledujici : ukPrvekSeznamu
 end;


type Seznam = object
begin

     public

     function prvniPrvek : IteratorSeznamu;

     procedure vloz( data: integer, pozice : IteratorSeznamu );

     procedure smaz( pozice : IteratorSeznamu );

     private
     hlavicka : ukPrvekSeznamu;

     posledni : ukPrvekSeznamu;

     pocetPrvku : integer;
end;

type Iterator = object

     function dalsi : boolean; virtual;

     function hodnota : integer; virtual;
end

type IteratorSeznamu = object (Iterator)
     function dalsi : boolean;

     function hodnota : integer;

     procedure nastav( data : integer );

     private
     prvek : ukPrvekSeznamu;
end

type IteratorStromu = object (Iterator)

function IteratorSeznamu. dalsi : boolean
begin
     prvek := prvek^. nasledujici;
     if prvek <> nil then
        dalsi := true;
     else
        dalsi := false;
end

function IteratorSeznam. hodnota : integer
begin
     hodnota := prvek^. data;
end

procedure filtruj( i : Iterator )
begin
     repeat
           if i. hodnota > 10 then
              i. nastav( 10 );
     until i. dalsi = false;
end

program Iterator2;
var seznam, seznam2, seznam3 : Seznam;
    i : IteratorSeznamu;
begin
     i = seznam. prvniPrvek;
     repeat
           writeln( " hodnota je ", i. hodnota );
           if i. hodnota < 10 then
              seznam. smaz( i );
     until i. dalsi = false;
end.

