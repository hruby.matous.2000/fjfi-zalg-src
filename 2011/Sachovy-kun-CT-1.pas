program sachovyKunCT;

type Sachovnice = object

     public

     {nuluje sachovnici}
     constructor vytvor;

     procedure zapisNaPolicko( i, j, hodnota : integer; );

     function nactiZPolicka( i, j: integer ) : integer;

     private

     sachovnice = array[ 1..8, 1..8 ] of integer;
end;

function prochazeniKonem( poziceI, poziceJ, krok : integer, var pole: Sachovnice ) : boolean
var pokus, novaPoziceI, novePoziceJ : boolean;
begin
  if krok = 65 then
     prochazeniKonem := true;
  else
  begin
     if( poziceI <= 8 and poziceI >= 1 and                 {zjistujeme, zda sem muzeme skocit}
         poziceJ <= 8 and poziceJ >=1 and
         pole. nactizpolicka( poziceI, poziceJ ) = 0 ) then
     begin
          pole. zapisnapolicko( poziceI, poziceJ, krok );  {skaceme na policko}
          for smer=1 to 8 do                               {zkousime skoky vsemi smery}
          begin
               case smer of:
               1: begin
                  novaPoziceI:=poziceI-1
                  novaPoziceJ:=poziceJ+2
               2: begin
                  novaPoziceI:=poziceI+1
                  novaPoziceJ:=poziceJ+2

                  ....

               pokus := prochazeniKonem( novaPoziceI, novaPoziceJ, krok + 1, pole);
               if pokus = true then
               begin
                  prochazeniKonem:=true;
                  exit;
               end
          end;
          pole. zapisnapolicko(poziceI,poziceJ, 0);           {vracime se zpet}
     end;
  end
  prochazeniKonem:=false;
end.

