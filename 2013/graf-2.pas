
type TGraf = object

public:

  constructor vytvor( jmenoSouboru: string );

  destructor smaz;

  function velikost() : integer; {vraci pocet uzlu}

  function hodnotaHrany( uzel1, uzel2 : integer ) : integer;

  function soused( uzel, cisloSouseda : integer ) : integer;

  function pocetSousedu( uzel : integer ) : integer;
  
end;

procedure Dijkstra( graf : TGraf,
                    start, cil : integer,
                    var cesta : array of integer )

type Stav [Neznamy,Navstiveny,Znamy];

var
  StavyUzlu: array of Stav;
  DelkaCestyDoUzlu: array of integer;
  PotencionalniUzel,i:integer;
  Soused, k: integer;
  Aktualni,NovaDelka, AktualniDelka: integer;
begin
  setlength(StavyUzlu,graf. velikost());
  setlength(DelkaCestyDoUzlu,graf. velikost());

  for i:=0 to graf.velikost-1 do
    begin
      StavyUzlu[i]:=Neznamy;
      DelkaCestyDoUzlu[i]:=maxinteger;
    end;

StavyUzlu[start]:=Navstiveny;
DelkaCestyDoUzlu[start]:=0;
while StavyUzlu[cil]<>Znamy do
  begin
  i:=0;
  while StavyUzlu[i]<>Navstiveny do inc(i);

  PotencionalniUzel:=i;  
    for i:=PotencionalniUzel to graf.velikost-1 do
      if (DelkaCestyDoUzlu[i]<DelkaCestyDoUzlu[PotencionalniUzel]) and
         (StavyUzlu[i]=Navstiveny) then PotencionalniUzel:=i;
  StavyUzlu[PotencionalniUzel]:=Znamy;
  AktualniDelka:=DelkaCestyDoUzlu[PotencionalniUzel];

  for i:=0 to Graf.PocetSousedu(PotencionalniUzel) do
      begin
      Soused:=Graf.Soused(PotencionalniUzel,i);
      NovaDelka:=AktualniDelka+ Graf.HodnotaHrany(PotencionalniUzel,Soused);
      StavyUzlu[i]:=Navstiveny;
      if DelkaCestyDoUzlu[Soused]>NovaDelka
            then DelkaCestyDoUzlu[Soused]:=NovaDelka;

      end;
  end;
  Aktualni:=cil;
  cesta[0]:=cil;
  k:=1;
  while Aktualni <> start do
  begin 
    for i:=0 to Graf.PocetSousedu(Aktualni) do
      begin
      Soused:=Graf.Soused(Aktualni,i);
      if DelkaCestyDoUzlu[Soused] = DelkaCestyDoUzlu[Aktualni] -
	  Graf.HodnotaHrany(Aktualni,Soused) then
	    begin
	    cesta[k]:=Soused;
	    Aktualni:=Soused;
	    inc(k);
	    break;
	    end;
      end;
   end;
   for i:=0 to (k-1) div 2 do 
    begin
    prohod(cesta, i, k-1-i);
    end;
end















