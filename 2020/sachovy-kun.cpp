#include <iostream>

////
// Obecne muzeme uvazovat i sochovnici o jinych rozmerech nez 8 * 8
// N je tedy velikost sachovnice
const int N = 8;

////
// TODO: doplnte dalsi skoky, kter muze provadet sachovy kun
int tahy[8][2] = {
	{-2,1},
	{-1,2}
};

////
// TODO: Napiste funkci, ktera otestuje, zda je mozne provest skok na pole
// se souradnicemi 'radek' a 'sloupec'. Tj. testujeme:
// 1. zda souradnice nejsou mimo sachovnici
// 2. zda jsme na dane policky jiz neskocili drive
//
bool testSkoku(bool sachovnice[ N ][ N ], int radek, int sloupec)
{
   if (radek < N && radek >= 0 &&
	sloupec >= 0 && sloupec < N && 
	sachovnice[radek][sloupec] != true)
	return true;
   return false;
}

////
// Toto je hlavni funkce pro reseni ulohy sachoveho kone pomoci backtrackingu. Pokud lze provest
// skok na policko dane pomoci parametru 'radek' a 'sloupec', skok se provede a zkousi se dalsi mozne cesty,
// kudy muze kun pokracovat. Pokud tento skok provest nelze, musime se vratit zpet. Funkce vraci 'true'
// pokud skok bylo mozne provest a 'false' jinak.
bool tahKonem( bool sachovnice[N][N],     // pole popisujici sachovnici, na ni si ukladame, ktera policka uz jsme prosli
               int cesta[ N * N ][ 2 ],   // pole kam ukladame pozice policek, na ktera kun postupne skace
               int radek,                 // pozice policka, na ktere chceme skocit v danem kroku
               int sloupec,               // pozice policka, na ktere chceme skocit v danem kroku
               int skok                   // udava, kolikaty skok kun udelal na sve ceste
               )
{
   // TODO 1. otestujte pomoci funkce 'testSkoku', zda muzeme provest skok na dane policko.
   
   // TODO 2. pokud ne koncime, jinak skok provedeme, tj. je potreba ho oznacit na sachovnici a zapsat do pole cesta.
   
   // TODO 3. Pokud uz jsme prosli celou sachovnici (zjistime podle poctu provedenych skoku), koncime.

   // TODO 4. Zkousime vsechny mozne skoky, ktere muze kun provest z tohoto policka pomoci REKURZIVNIHO volani 'tahKonem'
   // a pomoci pole 'tahy'. Pokud se vrati 'true' nasli jsme reseni.
   
   // TODO 5. Pokud se vrati false, pak tudy cesta k cili nevede amy se musim vratit o krok zpet, tj. musime na sachovnici
   // odmazat znacku, ze jsme navstivili policko se souradnicemi 'radek' a 'sloupec'.
}

int main()
{
   ////
   // Sachovnici reprezentujem pomoci 2D pole s rozmery N * N
   bool sachovnice[ N ][ N ];

   ////
   // Pole cesta obsahuje jednotlive skoky kone. Je to dvourozmerne pole takze
   // v k-tem kroku se kun nachazi na
   //
   // radek = cesta[ k ][ 0 ]
   // sloupec = cesta[ k ][ 1 ]
   int cesta[ N * N ][ 2 ];

   ////
   // Toto je pocatecni pozice kone
   const int startRadek = 3;
   const int startSloupec = 3;

   ////
   // TODO: Inicializujte sachovnici tak, aby odpovidala stavu, ze jsme
   // zatim neskocili na zdane policko

   tahKonem( sachovnice, cesta, startRadek, startSloupec, 0 );

   ////
   // TODO: Vypiste vyslednou cestu.
}


